// Command eccutil is for working with elliptic curves.
/*
Usage of ./eccutil:
  -decrypt
        decrypt the 'in' file to 'out', uses 'priv' key
  -encrypt
        encrypt the 'in' file to 'out', uses 'pub' key
  -in string
        input file (reads stdin by default)
  -newkeys int
        new key pair with bitsize [224,256,384,521]
        writes to 'pub' and 'priv' in PEM format
        if 'pass' is included the 'priv' key file will be encrypted
  -open
        opens the sealed 'in' file to 'out',
        uses 'pub' key of sender and 'priv' key of recipient
  -out string
        output file (writes stdout by default)
  -pass string
        password for private key file
  -priv string
        private key file in PEM format (default "private.pem")
  -pub string
        public key file in PEM format (default "public.pem")
  -seal
        seal the 'in' file to 'out',
        uses 'pub' key of recipient and 'priv' key of sender
  -sign
        reads a hash from 'in' in HEX format,
        writes signature to 'out' in HEX format, uses 'priv' key
  -verify
        reads a hash and signature from 'in' in HEX format,
        hash should be first and signature should be separated by whitespace,
        writes "verified" or "not verified" to 'out',
		uses signers 'pub' key
*/
/*
Some examples:

	$ ./eccutil -newkeys 256
	$ cat Walden.txt | ./eccutil -encrypt | ./eccutil -decrypt | diff Walden.txt -

	$ ./eccutil -newkeys 256 -pass abc123
	$ sha256sum Walden.txt > Walden.txt.sig
	$ cat Walden.txt.sig | ./eccutil -sign -pass abc123 >> Walden.txt.sig
	$ cat Walden.txt.sig
	9c5f66219b07b9d836f11080711ab09c63137891063add39bc2eacccbab8bfda  Walden.txt
	ffec8148f8627cf1b2f8412544f3e73f753ca0a66c901747333e0d8af587045dc995dbbaf3848aeae431d94562c35ea02387c57126f4b69c7bd11f65c1d72b28
	$ cat Walden.txt.sig | ./eccutil -verify -pass abc123
	Verification OK

	$ ./eccutil -newkeys 521
	$ ./eccutil -seal -in Walden.txt -out Walden.txt.sealed
	$ ./eccutil -open -in Walden.txt.sealed -out Walden.txt.opened
	$ diff Walden.txt Walden.txt.opened

*/
package main

import (
	"crypto/elliptic"
	"encoding/hex"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/elktree/ecc"
)

var (
	newkeySize = flag.Int("newkeys", 0, `new key pair with bitsize [224,256,384,521]
writes to 'pub' and 'priv' in PEM format
if 'pass' is included the 'priv' key file will be encrypted`)
	pubFile  = flag.String("pub", "public.pem", "public key file in PEM format")
	privFile = flag.String("priv", "private.pem", "private key file in PEM format")
	privPass = flag.String("pass", "", "password for private key file")
	inFile   = flag.String("in", "", "input file (reads stdin by default)")
	outFile  = flag.String("out", "", "output file (writes stdout by default)")
	signFlag = flag.Bool("sign", false, `reads a hash from 'in' in HEX format, 
writes signature to 'out' in HEX format, uses 'priv' key`)
	verifyFlag = flag.Bool("verify", false, `reads a hash and signature from 'in' in HEX format, 
hash should be first and signature should be separated by whitespace,
writes "Verification OK" or "Verification Failure" to 'out',
uses signers 'pub' key`)
	encryptFlag = flag.Bool("encrypt", false, "encrypt the 'in' file to 'out', uses 'pub' key")
	decryptFlag = flag.Bool("decrypt", false, "decrypt the 'in' file to 'out', uses 'priv' key")
	sealFlag    = flag.Bool("seal", false, `seal the 'in' file to 'out',
uses 'pub' key of recipient and 'priv' key of sender`)
	openFlag = flag.Bool("open", false, `opens the sealed 'in' file to 'out',
uses 'pub' key of sender and 'priv' key of recipient`)
)

////// util functions

func quit(msg ...interface{}) {
	fmt.Fprintln(os.Stderr, msg...)
	os.Exit(1)
}

func readFile(file *string) []byte {
	var in []byte
	var err error
	if *file == "" || *file == "-" {
		in, err = ioutil.ReadAll(os.Stdin)
	} else {
		in, err = ioutil.ReadFile(*file)
	}
	if err != nil {
		quit("read input:", err)
	}
	return in
}

func writeFile(file *string, out []byte) {
	var err error
	if *file == "" || *file == "-" {
		_, err = fmt.Print(string(out))
	} else {
		err = ioutil.WriteFile(*file, out, 0666)
	}
	if err != nil {
		quit("write output:", err)
	}
}

func readPublicKey() *ecc.PublicKey {
	pubBytes := readFile(pubFile)
	pub, err := ecc.DecodePEMPublicKey(pubBytes)
	if err != nil || pub == nil {
		quit("public key:", err)
	}
	return pub
}

func readPrivateKey() *ecc.PrivateKey {
	privBytes := readFile(privFile)
	priv, err := ecc.DecodePEMPrivateKey(privBytes, *privPass)
	if err != nil {
		quit("private key:", err)
	}
	return priv
}

func bit2curve(bits int) *elliptic.Curve {
	var c elliptic.Curve
	switch bits {
	case 224:
		c = elliptic.P224()
	case 256:
		c = elliptic.P256()
	case 384:
		c = elliptic.P384()
	case 521:
		c = elliptic.P521()
	}
	return &c
}

////// ec functions

func newkeys() {
	c := bit2curve(*newkeySize)
	if *c == nil {
		quit("no valid curve selected:", *newkeySize)
	}
	pub, priv, err := ecc.GenerateKeys(*c)
	if err != nil {
		quit("GenerateKeys:", err)
	}
	// write public
	pubBytes, err := pub.PEM()
	if err != nil {
		quit("public PEM:", err)
	}
	writeFile(pubFile, pubBytes)
	// write private
	privBytes, err := priv.PEM(*privPass)
	if err != nil {
		quit("private PEM:", err)
	}
	writeFile(privFile, privBytes)
}
func encrypt() {
	in := readFile(inFile)
	pub := readPublicKey()
	out, err := pub.Encrypt(in)
	if err != nil {
		quit("encrypt:", err)
	}
	writeFile(outFile, out)
}

func decrypt() {
	in := readFile(inFile)
	priv := readPrivateKey()
	out, err := priv.Decrypt(in)
	if err != nil {
		quit("decrypt:", err)
	}
	writeFile(outFile, out)
}

func sign() {
	var hash []byte
	var err error
	in := readFile(inFile)
	for _, f := range strings.Fields(string(in)) {
		hash, err = hex.DecodeString(strings.TrimSpace(f))
		if err == nil {
			break
		}
	}
	if len(hash) == 0 {
		quit("could not find HEX hash in input")
	}
	priv := readPrivateKey()
	sig, err := priv.Sign(hash)
	if err != nil {
		quit(err)
	}
	hexSig := make([]byte, hex.EncodedLen(len(sig)))
	hex.Encode(hexSig, sig)
	hexSig = append(hexSig, '\n')
	writeFile(outFile, hexSig)
}

func verify() {
	var hash []byte
	var sig []byte
	in := readFile(inFile)
	for _, f := range strings.Fields(string(in)) {
		hex, err := hex.DecodeString(strings.TrimSpace(f))
		if err == nil {
			if len(hash) == 0 {
				hash = hex
			} else {
				sig = hex
				break
			}
		}
	}
	if len(hash) == 0 {
		quit("could not find HEX hash in input")
	}
	if len(sig) == 0 {
		quit("could not find HEX signature in input")
	}
	pub := readPublicKey()
	verified, err := pub.Verify(hash, sig)
	if err != nil {
		quit(err)
	}
	v := []byte("Verification OK\n")
	if !verified {
		v = []byte("Verification Failure\n")
	}
	writeFile(outFile, v)
}

func seal() {
	in := readFile(inFile)
	pub := readPublicKey()
	priv := readPrivateKey()
	sealed, err := priv.Seal(in, pub)
	if err != nil {
		quit(err)
	}
	writeFile(outFile, sealed)
}

func open() {
	in := readFile(inFile)
	pub := readPublicKey()
	priv := readPrivateKey()
	opened, err := priv.Open(in, pub)
	if err != nil {
		quit(err)
	}
	writeFile(outFile, opened)
}

func main() {
	flag.Parse()

	switch {
	case *newkeySize > 0:
		newkeys()
	case *encryptFlag:
		encrypt()
	case *decryptFlag:
		decrypt()
	case *signFlag:
		sign()
	case *verifyFlag:
		verify()
	case *sealFlag:
		seal()
	case *openFlag:
		open()
	default:
		flag.Usage()
	}
}
