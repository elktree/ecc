# ecc elliptic curve cryptography

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/elktree/ecc)](https://goreportcard.com/report/gitlab.com/elktree/ecc)
[![Documentation](https://godoc.org/gitlab.com/elktree/ecc?status.svg)](http://godoc.org/gitlab.com/elktree/ecc)
[![Coverage Status](https://gocover.io/_badge/gitlab.com/elktree/ecc)](https://gocover.io/gitlab.com/elktree/ecc)
[![Build Status](https://gitlab.com/elktree/ecc/badges/master/build.svg)](https://gitlab.com/elktree/ecc/-/jobs)

Package ecc makes public key elliptic curve cryptography easier to use. Cryptographic functions are from "crypto/ecdsa" and "github.com/cloudflare/redoctober/ecdh".

    Sign/Verify                // authentication by signing a hash
    SignMessage/VerifyMessage  // authentication by hashing a message and signing the hash SHA256
    Encrypt/Decrypt            // encrypts with ephemeral symetrical key AES-128-CBC-HMAC-SHA1
    Seal/Open                  // both Sign and Encrypt, then Decrypt and Verify

    Marshal/Unmarshal          // convert keys to and from []byte slices
    PEM/DecodePEM              // keys in PEM file format, can be encrypted with a password

Packages ecdh, padding and symcrypt are copied from redoctober into this package so it works with go get. Only a few minor changes were made: package import paths, an error check and a comment added. Run ./diff_redoctober.sh to see all changes.

[Documentation and Examples](https://godoc.org/gitlab.com/elktree/ecc)

## Status

|          | Status       | Documentation | Examples | Tests    |
| -------- | ------------ | :-----------: | :------: | :------: |
| Package  | alpha/stable | X             | X        | X        |
| eccutil  | alpha/stable | X             | X        |          |
| libecc   | alpha        |               |          | driver.c |

## eccutil

There is a command line utility in "cmd/eccutil".
[Usage and Examples](https://godoc.org/gitlab.com/elktree/ecc/cmd/eccutil)

    go install gitlab.com/elktree/ecc/cmd/eccutil

## libecc

libecc is a C wrapper for package ecc.

    go build -buildmode=c-shared

The windows directory has a batch file for building: libecc.def, libecc.lib and libecc.dll. There is also a test file driver.c

If you're on linux try setting the library path: `LD_LIBRARY_PATH=. ./a.out`

### Build with Go 1.11.5 or 1.10.8 (or later)

> This DoS vulnerability in the crypto/elliptic implementations of the P-521 and P-384 elliptic curves may let an attacker craft inputs that consume excessive amounts of CPU.

### Message in a bottle

Photo by Guilherme Stecanella on Unsplash