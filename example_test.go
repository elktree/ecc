package ecc_test

import (
	"bytes"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"fmt"

	"gitlab.com/elktree/ecc"
)

func Example_encrypt() {
	// create keys
	pub, priv, _ := ecc.GenerateKeys(elliptic.P521())

	plaintext := "secret secrets are no fun, secret secrets hurt someone"

	// use public key to encrypt
	encrypted, _ := pub.Encrypt([]byte(plaintext))
	fmt.Println(len(encrypted))

	// use private key to decrypt
	decrypted, _ := priv.Decrypt(encrypted)
	fmt.Println(string(decrypted))

	// Output:
	// 234
	// secret secrets are no fun, secret secrets hurt someone
}

func Example_signVerify() {
	// create keys
	pub, priv, _ := ecc.GenerateKeys(elliptic.P224())

	// get random bytes as plaintext
	plaintextBytes := make([]byte, 2500)
	if n, err := rand.Reader.Read(plaintextBytes); n != 2500 || err != nil {
		panic("failed to read random bytes")
	}
	hash := sha256.Sum256(plaintextBytes)

	// Sign the hash with the senders private key.
	sig, _ := priv.Sign(hash[:]) // hash is type [32]byte, need []byte

	// send plaintext and sig

	// Verify the signature with the senders public key. The recipient should
	// create the hash from the plaintext (here we reuse the hash).
	verified, _ := pub.Verify(hash[:], sig)
	fmt.Println(verified)

	// Output:
	// true
}

func Example_signMessage() {
	// create keys
	pub, priv, _ := ecc.GenerateKeys(elliptic.P384())

	plaintext := "secret secrets are no fun, secret secrets hurt someone"

	// Sign the message with the senders private key.
	sig, _ := priv.SignMessage([]byte(plaintext))

	// send plaintext and sig

	// Verify the signature with the senders public key.
	verified, _ := pub.VerifyMessage([]byte(plaintext), sig)
	fmt.Println(verified)

	// Output:
	// true
}

func Example_pEM() {
	const privPem = `
-----BEGIN EC PRIVATE KEY-----
MIGkAgEBBDAED2R9NFkyj4E5ITbx20iSiYyHVxvxl7awC1aPfZ49KunnssO97+07
R7qyhIUZ6aCgBwYFK4EEACKhZANiAAShYa1f7DltZ2tDBHs6MdgMt3MV8u0DOIC+
ZZ3LjvP5corPnvkXgJI/0np63Zm09KsEbK/JB/tL6Lzp30ROxRbmOuxGSbL353jD
zaXKbGmizJi2+Vr/0BQKXEM0h2laHDU=
-----END EC PRIVATE KEY-----
`
	// load keys (the private key contains the public key)
	priv, _ := ecc.DecodePEMPrivateKey([]byte(privPem), "")

	// ecc.Keys are wrappers around ecdsa.Keys
	pub := ecc.PublicKey{Key: &priv.Key.PublicKey}

	// get public key in PEM format
	pubPem, _ := pub.PEM()
	fmt.Println(string(pubPem))

	// Output:
	// -----BEGIN PUBLIC KEY-----
	// MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEoWGtX+w5bWdrQwR7OjHYDLdzFfLtAziA
	// vmWdy47z+XKKz575F4CSP9J6et2ZtPSrBGyvyQf7S+i86d9ETsUW5jrsRkmy9+d4
	// w82lymxposyYtvla/9AUClxDNIdpWhw1
	// -----END PUBLIC KEY-----
}

func Example_encyptedPEM() {
	const encryptedPem = `
-----BEGIN EC PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,94853fd16a027c0c07663ffc09cc32e6

cclqDp6HTJD/xB/nk3fDBDJLnSohMLnsjasx4l8QaD0hMICg/r4gtPHuzDHwxLGA
GHNahxeZkDYq/RutOjcihihC5FKdJJZ1b88EnXoprG7Kp2iwbPteD6mbzWqxwpbJ
aP0xnZAk+xghjHb9v/qxG2lFho/JpGIek8SfDLX5eH0=
-----END EC PRIVATE KEY-----
`

	// load private key with password "abc123"
	priv, _ := ecc.DecodePEMPrivateKey([]byte(encryptedPem), "abc123")

	// get PEM without encryption; password is blank
	plainPem, _ := priv.PEM("")
	fmt.Println(string(plainPem))

	// Output:
	// -----BEGIN EC PRIVATE KEY-----
	// MHcCAQEEIFtZ6cvIvTN2yGlL/VDL0hfQCrui+wEg5/kt6X1KgfiBoAoGCCqGSM49
	// AwEHoUQDQgAE9rgmY7NQAqt4r3o2Bt4ViS8HWlHHJ+Ig2VIpRaxUyMrrZma9hHu5
	// SvdpYhDA1fClYKw0ZEKkuZ5xZFtCJ4/MkQ==
	// -----END EC PRIVATE KEY-----
}

func Example_marshal() {
	const privPem = `
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIFtZ6cvIvTN2yGlL/VDL0hfQCrui+wEg5/kt6X1KgfiBoAoGCCqGSM49
AwEHoUQDQgAE9rgmY7NQAqt4r3o2Bt4ViS8HWlHHJ+Ig2VIpRaxUyMrrZma9hHu5
SvdpYhDA1fClYKw0ZEKkuZ5xZFtCJ4/MkQ==
-----END EC PRIVATE KEY-----
`
	// load keys (the private key contains the public key)
	priv, _ := ecc.DecodePEMPrivateKey([]byte(privPem), "")

	// ecc.Keys are wrappers around ecdsa.Keys
	pub := ecc.PublicKey{Key: &priv.Key.PublicKey}

	// A single byte (x04) is prefixed to the raw public key bytes when marshalled,
	// indicating that the key is uncompressed. This makes a P-256 public key 65
	// bytes instead of 32 bytes for X and 32 bytes for Y.
	pubMarshalled := pub.Marshal()
	fmt.Printf("public key len(%d): %x...\n", len(pubMarshalled), pubMarshalled[:20])

	// Marshaled private key (in x509 ASN.1, DER format) is 121 bytes. It stores
	// the 32 bytes of private key, 65 bytes of public key, the curve ID and a
	// version number.
	privMarshalled, _ := priv.Marshal()
	fmt.Printf("private key len(%d): %x...\n", len(privMarshalled), privMarshalled[:20])

	priv2, _ := ecc.UnmarshalPrivateKey(privMarshalled)
	fmt.Println(priv2.Key.Params().Name)

	pub2 := ecc.UnmarshalPublicKey(elliptic.P256(), pubMarshalled)
	fmt.Println(&pub == pub2) // false. same values but not the same pointers

	// Output:
	// public key len(65): 04f6b82663b35002ab78af7a3606de15892f075a...
	// private key len(121): 307702010104205b59e9cbc8bd3376c8694bfd50...
	// P-256
	// false
}

func Example_sealOpen() {
	// create keys, NB: different curves are used for sender and receiver
	sendersPub, sendersPriv, _ := ecc.GenerateKeys(elliptic.P224())
	receiversPub, receiversPriv, _ := ecc.GenerateKeys(elliptic.P521())

	// get random bytes as plaintext
	plaintextBytes := make([]byte, 8192)
	if n, err := rand.Reader.Read(plaintextBytes); n != 8192 || err != nil {
		panic("failed to read random bytes")
	}

	// seal both signs and encrypts
	sealed, _ := sendersPriv.Seal(plaintextBytes, receiversPub)

	// send the sealed bytes

	// open decrypts and verifies the senders signature
	opened, _ := receiversPriv.Open(sealed, sendersPub)

	compared := bytes.Compare(plaintextBytes, opened)
	fmt.Println(compared == 0)

	// Output:
	// true
}
