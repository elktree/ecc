// Package ecc makes public key elliptic curve cryptography easier to use.
// Cryptographic functions are from "crypto/ecdsa" and
// "github.com/cloudflare/redoctober/ecdh".
//
// 	Sign/Verify                // authentication by signing a hash
// 	SignMessage/VerifyMessage  // authentication by hashing a message and signing the hash SHA256
// 	Encrypt/Decrypt            // encrypts with ephemeral symmetrical key AES-128-CBC-HMAC-SHA1
// 	Seal/Open                  // both Sign and Encrypt, then Decrypt and Verify
//
// 	Marshal/Unmarshal          // convert keys to and from []byte slices
// 	PEM/DecodePEM              // keys in PEM file format, can be encrypted with a password
//
// Packages ecdh, padding and symcrypt are copied from redoctober into this
// package so it works with go get. Only a few minor changes were made: package
// import paths, an error check and a comment added. Run ./diff_redoctober.sh to
// see all changes.
package ecc

import (
	"bytes"
	"crypto/elliptic"
	"testing"
)

func TestGenerateKeys(t *testing.T) {
	tests := []struct {
		name      string
		curve     elliptic.Curve
		want      int
		wantMatch bool
		wantErr   bool
	}{
		{"create P224", elliptic.P224(), 224, true, false},
		{"create P256", elliptic.P256(), 256, true, false},
		{"create P384", elliptic.P384(), 384, true, false},
		{"create P521", elliptic.P521(), 521, true, false},
		{"wrong bitsize", elliptic.P256(), 999, false, false},
		{"nil curve", elliptic.Curve(nil), 999, false, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := GenerateKeys(tt.curve)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateKeys() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantMatch && (got.Key.Params().BitSize != tt.want) {
				t.Errorf("GenerateKeys() got = %d, want %d", got.Key.Params().BitSize, tt.want)
			}
		})
	}
}

func TestPublicKey_Verify(t *testing.T) {
	hash := []byte("0123456789abcdef")
	bigHash := []byte(`0123456789abcdef0123456789abcdef0123456789
abcdef0123456789abcdef0123456789abcdefabcdef0123456789abcdef0123456789abcdef
abcdef0123456789abcdef0123456789abcdefabcdef0123456789abcdef0123456789abcdef
abcdef0123456789abcdef0123456789abcdefabcdef0123456789abcdef0123456789abcdef
abcdef0123456789abcdef0123456789abcdefabcdef0123456789abcdef0123456789abcdef
abcdef0123456789abcdef0123456789abcdefabcdef0123456789abcdef0123456789abcdef`)
	tests := []struct {
		name      string
		hash1     []byte
		hash2     []byte
		signature []byte
		want      bool
		wantErr   bool
	}{
		{"same hash", hash, hash, nil, true, false},
		{"different hash", hash, []byte("different_hash"), nil, false, false},
		{"no hash", []byte{}, []byte(""), nil, false, true},
		{"small hash", []byte("a"), []byte("a"), nil, true, false},
		{"big hash", bigHash, bigHash, nil, true, false},
		{"nil hash", nil, nil, nil, false, true},
		{"no signature", hash, hash, []byte{}, false, true},
		{"empty signature", hash, hash, []byte{0, 0}, false, true},
		{"malformed signature", hash, hash, []byte{'q', 4}, false, true},
	}
	for _, tt := range tests {
		pub, priv, _ := GenerateKeys(elliptic.P256())
		if tt.signature == nil {
			tt.signature, _ = priv.Sign(tt.hash1)
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := pub.Verify(tt.hash2, tt.signature)
			if (err != nil) != tt.wantErr {
				t.Errorf("PublicKey.Verify() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PublicKey.Verify() = %v, want %v", got, tt.want)
			}
		})
	}
}

var bigMessage = `
	Sometimes, having had a surfeit of human society and gossip, and
worn out all my village friends, I rambled still farther westward
than I habitually dwell, into yet more unfrequented parts of the
town, "to fresh woods and pastures new," or, while the sun was
setting, made my supper of huckleberries and blueberries on Fair
Haven Hill, and laid up a store for several days.  The fruits do not
yield their true flavor to the purchaser of them, nor to him who
raises them for the market.  There is but one way to obtain it, yet
few take that way.  If you would know the flavor of huckleberries,
ask the cowboy or the partridge.  It is a vulgar error to suppose
that you have tasted huckleberries who never plucked them.  A
huckleberry never reaches Boston; they have not been known there
since they grew on her three hills.  The ambrosial and essential
part of the fruit is lost with the bloom which is rubbed off in the
market cart, and they become mere provender.  As long as Eternal
Justice reigns, not one innocent huckleberry can be transported
thither from the country's hills.`

func TestPublicKey_VerifyMessage(t *testing.T) {
	message := `When the ponds were firmly frozen, they afforded not only new
and shorter routes to many points, but new views from their surfaces
of the familiar landscape around them.`
	tests := []struct {
		name      string
		message1  string
		message2  string
		signature []byte
		want      bool
		wantErr   bool
	}{
		{"same message", message, message, nil, true, false},
		{"different message", message, "different message", nil, false, false},
		{"no message", "", "", nil, true, false},
		{"short message", "a", "a", nil, true, false},
		{"big message", bigMessage, bigMessage, nil, true, false},
		{"nil/no message", string([]byte(nil)), string([]byte(nil)), nil, true, false},
		{"no signature", message, message, []byte{}, false, true},
		{"empty signature", message, message, []byte{0, 0}, false, true},
		{"malformed signature", message, message, []byte{'q', 4}, false, true},
	}
	for _, tt := range tests {
		pub, priv, _ := GenerateKeys(elliptic.P256())
		if tt.signature == nil {
			tt.signature, _ = priv.SignMessage([]byte(tt.message1))
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := pub.VerifyMessage([]byte(tt.message2), tt.signature)
			if (err != nil) != tt.wantErr {
				t.Errorf("PublicKey.VerifyMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PublicKey.VerifyMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPublicKey_Encrypt(t *testing.T) {
	message := []byte(`Ancient poetry and mythology suggest, at least, that husbandry
	was once a sacred art; but it is pursued with irreverent haste and
	heedlessness by us, our object being to have large farms and large
	crops merely.  We have no festival, nor procession, nor ceremony,
	not excepting our cattle-shows and so-called Thanksgivings, by which
	the farmer expresses a sense of the sacredness of his calling, or is
	reminded of its sacred origin.`)
	tests := []struct {
		name      string
		message   []byte
		minLength int
		pub       *PublicKey
		wantErr   bool
	}{
		{"message", message, 550, nil, false},
		{"short message", []byte("a"), 118, nil, false},
		{"big message", []byte(bigMessage), 118, nil, false},
		{"empty message", []byte{}, 118, nil, false},
		{"nil message", nil, 118, nil, false},
		{"nil key", message, 0, &PublicKey{}, true},
	}
	for _, tt := range tests {
		if tt.pub == nil {
			tt.pub, _, _ = GenerateKeys(elliptic.P256())
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.pub.Encrypt(tt.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("PublicKey.Encrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) < tt.minLength {
				t.Errorf("PublicKey.Encrypt() = len(%d), minLength %d", len(got), tt.minLength)
			}
		})
	}
}

func TestPrivateKey_Sign(t *testing.T) {
	hash := []byte("0123456789abcdef")
	tests := []struct {
		name    string
		priv    *PrivateKey
		hash    []byte
		wantErr bool
	}{
		{"sign", nil, hash, false},
		{"no hash", nil, nil, false},
		{"sign bigMessage", nil, []byte(bigMessage), false},
		{"bad key", &PrivateKey{}, hash, true},
		{"bad key", &PrivateKey{Key: nil}, nil, true},
	}
	for _, tt := range tests {
		if tt.priv == nil {
			_, tt.priv, _ = GenerateKeys(elliptic.P256())
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.priv.Sign(tt.hash)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.Sign() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(got) < signatureMinLen {
				t.Errorf("PrivateKey.Sign() Signature = len(%d)", len(got))
			}
		})
	}
}

func TestPrivateKey_SignMessage(t *testing.T) {
	message := []byte(`Every morning was a cheerful invitation to make my life of equal
	simplicity, and I may say innocence, with Nature herself.`)
	tests := []struct {
		name    string
		priv    *PrivateKey
		message []byte
		wantErr bool
	}{
		{"sign", nil, message, false},
		{"no message", nil, nil, false},
		{"sign bigMessage", nil, []byte(bigMessage), false},
		{"bad key", &PrivateKey{}, message, true},
		{"bad key", &PrivateKey{Key: nil}, nil, true},
	}
	for _, tt := range tests {
		if tt.priv == nil {
			_, tt.priv, _ = GenerateKeys(elliptic.P256())
		}
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.priv.SignMessage(tt.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.SignMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestPrivateKey_Decrypt(t *testing.T) {
	message := []byte(`I heartily accept the motto, -- "That government is best which
	governs least"; and I should like to see it acted up to more rapidly
	and systematically.  Carried out, it finally amounts to this, which
	also I believe, -- "That government is best which governs not at
	all"; and when men are prepared for it, that will be the kind of
	government which they will have.`)
	tests := []struct {
		name      string
		pub       *PublicKey
		priv      *PrivateKey
		message   []byte
		encrypted []byte
		wantErr   bool
	}{
		{"encrypt", nil, nil, message, nil, false},
		{"nil message", nil, nil, nil, nil, false},
		{"bad encrypted", nil, nil, message, []byte("0123456789abcdef"), true},
		{"bad encrypted", nil, nil, message, []byte(""), true},
		{"bad encrypted", nil, nil, nil, []byte("0123456789abcdef"), true},
	}
	for _, tt := range tests {
		pub, priv, _ := GenerateKeys(elliptic.P256())
		if tt.pub == nil {
			tt.pub = pub
		}
		if tt.priv == nil {
			tt.priv = priv
		}
		if tt.encrypted == nil {
			tt.encrypted, _ = pub.Encrypt(tt.message)
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.priv.Decrypt(tt.encrypted)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.Decrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && bytes.Compare(got, tt.message) != 0 {
				t.Errorf("PrivateKey.Decrypt() = %v, want %v", got, tt.message)
			}
		})
	}
}

func TestPrivateKey_Seal(t *testing.T) {
	message := []byte(`I think that we
	should be men first, and subjects afterward.  It is not desirable to
	cultivate a respect for the law, so much as for the right.  The only
	obligation which I have a right to assume is to do at any time what
	I think right.  It is truly enough said that a corporation has no
	conscience; but a corporation of conscientious men is a corporation
	with a conscience.  Law never made men a whit more just; and, by
	means of their respect for it, even the well-disposed are daily made
	the agents of injustice.`)
	tests := []struct {
		name    string
		message []byte
		toPub   *PublicKey
		wantErr bool
	}{
		{"seal", message, nil, false},
		{"nil message", nil, nil, false},
		{"bad key", message, &PublicKey{}, true},
	}
	for _, tt := range tests {
		_, fromPriv, _ := GenerateKeys(elliptic.P256())
		if tt.toPub == nil {
			tt.toPub, _, _ = GenerateKeys(elliptic.P256())
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := fromPriv.Seal(tt.message, tt.toPub)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.Seal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			sig, _ := fromPriv.SignMessage(message)
			if err == nil && len(got) < len(sig) {
				t.Errorf("PrivateKey.Seal() too short = len(%d), min(%d)", len(got), len(sig))
			}
		})
	}
}

func TestPrivateKey_Open(t *testing.T) {
	message := []byte(`
There are thousands who are in opinion opposed to slavery and to the
war, who yet in effect do nothing to put an end to them; who,
esteeming themselves children of Washington and Franklin, sit down
with their hands in their pockets, and say that they know not what
to do, and do nothing; who even postpone the question of freedom to
the question of free-trade, and quietly read the prices-current
along with the latest advices from Mexico, after dinner, and, it may
be, fall asleep over them both.  What is the price-current of an
honest man and patriot to-day?  They hesitate, and they regret, and
sometimes they petition; but they do nothing in earnest and with
effect.  They will wait, well disposed, for others to remedy the
evil, that they may no longer have it to regret.  At most, they give
only a cheap vote, and a feeble countenance and Godspeed, to the
right, as it goes by them.
`)
	tests := []struct {
		name    string
		message []byte
		toPriv  *PrivateKey
		wantErr bool
	}{
		{"bad message", message, nil, true},
		{"nil message", nil, nil, true},
		{"bad key", message, &PrivateKey{}, true},
	}
	for _, tt := range tests {
		fromPub, _, _ := GenerateKeys(elliptic.P256())
		if tt.toPriv == nil {
			_, tt.toPriv, _ = GenerateKeys(elliptic.P256())
		}
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.toPriv.Open(tt.message, fromPub)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.Open() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
