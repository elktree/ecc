package ecc

import (
	"crypto/elliptic"
	"reflect"
	"testing"
)

func TestPublicKey_Marshal(t *testing.T) {
	pub, _, _ := GenerateKeys(elliptic.P256())
	tests := []struct {
		name    string
		pub     *PublicKey
		wantErr bool
	}{
		{"marshal length", pub, false},
		{"bad key", &PublicKey{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.pub.Marshal()
			if !tt.wantErr {
				if len(got) <= 1 {
					t.Errorf("PublicKey.Marshal() = len(%d)", len(got))
				} else if got[0] != 4 {
					t.Errorf("PublicKey.Marshal() = no leading x04 for uncompressed")
				} else if len(got) != 65 {
					t.Errorf("PublicKey.Marshal() = len() not == 65")
				}
			}
			if tt.wantErr && len(got) != 0 {
				t.Errorf("PublicKey.Marshal() = len(%d) should be 0", len(got))
			}
		})
	}
}

func TestPrivateKey_Marshal(t *testing.T) {
	_, priv, _ := GenerateKeys(elliptic.P256())
	tests := []struct {
		name    string
		priv    *PrivateKey
		wantErr bool
	}{
		{"marshal", priv, false},
		{"bad key", &PrivateKey{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.priv.Marshal()
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestUnmarshalPublicKey(t *testing.T) {
	type args struct {
		curve         elliptic.Curve
		marshalledKey []byte
	}
	tests := []struct {
		name string
		args args
		want *PublicKey
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UnmarshalPublicKey(tt.args.curve, tt.args.marshalledKey); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnmarshalPublicKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnmarshalPrivateKey(t *testing.T) {
	type args struct {
		marshalledKey []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *PrivateKey
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnmarshalPrivateKey(tt.args.marshalledKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalPrivateKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnmarshalPrivateKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
