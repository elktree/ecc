package ecc

import (
	"reflect"
	"testing"
)

const privPem = `
-----BEGIN PRIVATE KEY-----
MIGkAgEBBDAED2R9NFkyj4E5ITbx20iSiYyHVxvxl7awC1aPfZ49KunnssO97+07
R7qyhIUZ6aCgBwYFK4EEACKhZANiAAShYa1f7DltZ2tDBHs6MdgMt3MV8u0DOIC+
ZZ3LjvP5corPnvkXgJI/0np63Zm09KsEbK/JB/tL6Lzp30ROxRbmOuxGSbL353jD
zaXKbGmizJi2+Vr/0BQKXEM0h2laHDU=
-----END PRIVATE KEY-----
`

func TestPublicKey_PEM(t *testing.T) {
	tests := []struct {
		name    string
		pub     *PublicKey
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.pub.PEM()
			if (err != nil) != tt.wantErr {
				t.Errorf("PublicKey.PEM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PublicKey.PEM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPrivateKey_PEM(t *testing.T) {
	type args struct {
		password string
	}
	tests := []struct {
		name    string
		priv    *PrivateKey
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.priv.PEM(tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrivateKey.PEM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PrivateKey.PEM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodePEMPublicKey(t *testing.T) {
	type args struct {
		pemEncodedKey []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *PublicKey
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodePEMPublicKey(tt.args.pemEncodedKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodePEMPublicKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodePEMPublicKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodePEMPrivateKey(t *testing.T) {
	type args struct {
		pemEncodedKey []byte
		password      string
	}
	tests := []struct {
		name    string
		args    args
		want    *PrivateKey
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodePEMPrivateKey(tt.args.pemEncodedKey, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodePEMPrivateKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodePEMPrivateKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
