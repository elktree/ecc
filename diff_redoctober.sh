#!/usr/bin/env /bin/sh

# If you don't have the redoctober source, run:
#
# go get -u github.com/cloudflare/redoctober/ecdh/...

cd $GOPATH/src

diff ecc/symcrypt/symcrypt.go github.com/cloudflare/redoctober/symcrypt/symcrypt.go
diff ecc/padding/padding.go github.com/cloudflare/redoctober/padding/padding.go
diff ecc/ecdh/ecdh.go github.com/cloudflare/redoctober/ecdh/ecdh.go

diff ecc/symcrypt/symcrypt_test.go github.com/cloudflare/redoctober/symcrypt/symcrypt_test.go
diff ecc/padding/padding_test.go github.com/cloudflare/redoctober/padding/padding_test.go
diff ecc/ecdh/ecdh_test.go github.com/cloudflare/redoctober/ecdh/ecdh_test.go
